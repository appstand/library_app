package com.appstand.library.model

data class User(
    val userName: String
)