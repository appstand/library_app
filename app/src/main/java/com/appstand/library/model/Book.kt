package com.appstand.library.model

data class Book(
    val documentId: String,
    val label: String,
    val isLent: Boolean,
    val ownerUserName: String,
    val lentToUserName: String
)