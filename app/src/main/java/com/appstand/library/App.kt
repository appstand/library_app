package com.appstand.library

import android.app.Application
import android.util.Log
import com.appstand.library.model.User

class App : Application() {

    var signedInUser: User? = null

    fun signInUser(userName: String) {
        Log.d("App", "signIn: $userName")
        signedInUser = User(userName)
    }
}