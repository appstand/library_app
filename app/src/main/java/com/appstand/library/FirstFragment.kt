package com.appstand.library

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.appstand.library.databinding.FragmentFirstBinding
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    companion object {
        const val TAG = "FirstFragment"
    }

    private val db = Firebase.firestore
    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginExistingUser.setOnClickListener {
            loginUser(binding.editTextExistingUser.text.toString())
        }

        binding.registerNewUser.setOnClickListener {
            registerNewUser(binding.editTextRegisterUser.text.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun loginUser(userName: String) {
        binding.progressLoader.visibility = View.VISIBLE
        Log.d(TAG, "invoke")
        db.collection(FIRESTORE_USERS_COLLECTION)
            .whereEqualTo(FIRESTORE_USER_NAME, userName)
            .get()
            .addOnSuccessListener { result ->
                Log.d(TAG, "LOGIN RESULT: $result")
                if (result.isEmpty) {
                    binding.progressLoader.visibility = View.GONE
                    Toast.makeText(context, "User missing!", Toast.LENGTH_SHORT).show()
                }
                for (document in result) {
                    Log.d(TAG, "LOGIN ${document.id} => ${document.data}")
                    (requireContext().applicationContext as App).signInUser(userName)
                    findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
                    binding.progressLoader.visibility = View.GONE
                }
            }
            .addOnFailureListener { exception ->
                binding.progressLoader.visibility = View.GONE
                Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show()
                Log.w(TAG, "LOGIN Error getting documents.", exception)
            }
    }

    private fun registerNewUser(userName: String) {
        binding.progressLoader.visibility = View.VISIBLE
        Log.d(TAG, "invoke")
        db.collection(FIRESTORE_USERS_COLLECTION)
            .whereEqualTo(FIRESTORE_USER_NAME, userName)
            .get()
            .addOnSuccessListener { result ->
                Log.d(TAG, "REGISTER RESULT: $result")
                if (result.isEmpty) {
                    addUserToDataStore(userName)
                } else {
                    binding.progressLoader.visibility = View.GONE
                    Toast.makeText(context, "User already exists!", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { exception ->
                binding.progressLoader.visibility = View.GONE
                Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show()
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    private fun addUserToDataStore(userName: String) {
        Log.d(TAG, "addUser invoked")
        val data = hashMapOf(
            FIRESTORE_USER_NAME to userName,
        )
        db.collection(FIRESTORE_USERS_COLLECTION)
            .add(data)
            .addOnSuccessListener { documentReference ->
                (requireContext().applicationContext as App).signInUser(userName)
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
                binding.progressLoader.visibility = View.GONE
                Log.d(TAG, "AddUser DocumentSnapshot written with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                binding.progressLoader.visibility = View.GONE
                Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show()
                Log.w(TAG, "AddUser Error adding document", e)
            }
    }
}