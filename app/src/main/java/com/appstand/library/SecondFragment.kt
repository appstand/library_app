package com.appstand.library

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.appstand.library.databinding.FragmentSecondBinding
import com.appstand.library.model.Book
import com.appstand.library.model.User
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    companion object {
        const val TAG = "SecondFragment"
    }

    private val db = Firebase.firestore
    private var _binding: FragmentSecondBinding? = null
    private lateinit var currentLoggedInUser: User

    private lateinit var adapter: BookAdapter
    private var currentTab: Int = 1

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currentLoggedInUser = (requireContext().applicationContext as App).signedInUser!!
        adapter = BookAdapter() { bookId ->
            when (currentTab) {
                1 -> returnBook(bookId)
                2 -> borrowBook(bookId)
                3 -> returnBook(bookId)
            }
        }
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter

        binding.bottomNavigation.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.page_1 -> {
                    getMyAvailableBooks()
                    currentTab = 1
                    true
                }
                R.id.page_3 -> {
                    getAvailableBooksNearMe()
                    currentTab = 2
                    true
                }
                R.id.page_4 -> {
                    getBooksThatIHaveLentFromSomeone()
                    currentTab = 3
                    true
                }
                else -> false
            }
        }

        binding.floatingAddButton.setOnClickListener {
            showdialog()
        }

        getMyAvailableBooks()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getMyAvailableBooks() {
        binding.floatingAddButton.isVisible = true
        Log.d(TAG, "getMyAvailableBooks invoke")
        db.collection(FIRESTORE_BOOKS_COLLECTION)
            .whereEqualTo(FIRESTORE_BOOK_OWNER_USER_NAME, currentLoggedInUser.userName)
            .get()
            .addOnSuccessListener { result ->
                if (result.isEmpty) {
                    adapter.submitList(listOf())
                }
                adapter.submitList(result.documents.map { document ->
                    Log.d(TAG, "getMyAvailableBooks ${document.id} => ${document.data}")
                    document.toBook()
                }
                )
            }
            .addOnFailureListener { exception ->
                // TODO NO IDEA WHAT TO DO WHEN HAPPENS
                Log.w(TAG, "getMyAvailableBooks Error getting documents.", exception)
            }
    }

    private fun getAvailableBooksNearMe() {
        binding.floatingAddButton.isVisible = false
        Log.d(TAG, "getBooksIHaveBorrowed invoke")
        db.collection(FIRESTORE_BOOKS_COLLECTION)
            .whereNotEqualTo(FIRESTORE_BOOK_OWNER_USER_NAME, currentLoggedInUser.userName)
            .get()
            .addOnSuccessListener { result ->
                if (result.isEmpty) {
                    adapter.submitList(listOf())
                }
                adapter.submitList(result.documents.map { document ->
                    Log.d(TAG, "getBooksIHaveBorrowed ${document.id} => ${document.data}")
                    document.toBook()
                }.filter { !it.isLent }
                )
            }
            .addOnFailureListener { exception ->
                // TODO NO IDEA WHAT TO DO WHEN HAPPENS
                Log.w(TAG, "getBooksIHaveBorrowed Error getting documents.", exception)
            }
    }

    private fun getBooksThatIHaveLentFromSomeone() {
        binding.floatingAddButton.isVisible = false
        Log.d(TAG, "getBooksThatIHaveLentFromSomeone invoke")
        db.collection(FIRESTORE_BOOKS_COLLECTION)
            .whereEqualTo(FIRESTORE_BOOK_LENT_TO_USER_NAME, currentLoggedInUser.userName)
            .get()
            .addOnSuccessListener { result ->
                if (result.isEmpty) {
                    adapter.submitList(listOf())
                }
                adapter.submitList(result.documents.map { document ->
                    Log.d(
                        TAG,
                        "getBooksThatIHaveLentFromSomeone ${document.id} => ${document.data}"
                    )
                    document.toBook()
                }.filter { it.isLent }
                )
            }
            .addOnFailureListener { exception ->
                // TODO NO IDEA WHAT TO DO WHEN HAPPENS
                Log.w(TAG, "getBooksThatIHaveLentFromSomeone Error getting documents.", exception)
            }
    }

    fun showdialog() {
        val builder: AlertDialog.Builder = android.app.AlertDialog.Builder(requireContext())
        builder.setTitle("Title")

        val input = EditText(requireContext())
        input.setHint("Enter Text")
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(input)

        builder.setPositiveButton("OK") { dialog, which ->
            val m_Text = input.text.toString()
            addBook(m_Text)
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel", { dialog, which -> dialog.cancel() })

        builder.show()
    }

    private fun addBook(bookName: String) {
        Log.d(FirstFragment.TAG, "addBook invoked")
        val data = hashMapOf(
            FIRESTORE_BOOK_LABEL to bookName,
            FIRESTORE_BOOK_IS_LENT to false,
            FIRESTORE_BOOK_OWNER_USER_NAME to currentLoggedInUser.userName,
            FIRESTORE_BOOK_LENT_TO_USER_NAME to ""
        )
        db.collection(FIRESTORE_BOOKS_COLLECTION)
            .add(data)
            .addOnSuccessListener { documentReference ->
                getMyAvailableBooks()
                Log.d(
                    FirstFragment.TAG,
                    "addBook DocumentSnapshot written with ID: ${documentReference.id}"
                )
            }
            .addOnFailureListener { e ->
                // TODO SHOW SOMETHING WENT WRONG,  REMOVE LOADING SPINNER
                Log.w(FirstFragment.TAG, "addBook Error adding document", e)
            }
    }

    private fun borrowBook(bookId: String) {
        db.collection(FIRESTORE_BOOKS_COLLECTION)
            .document(bookId)
            .update(
                mapOf(
                    FIRESTORE_BOOK_IS_LENT to true,
                    FIRESTORE_BOOK_LENT_TO_USER_NAME to currentLoggedInUser.userName
                )
            )
        getAvailableBooksNearMe()
    }

    private fun returnBook(bookId: String) {
        db.collection(FIRESTORE_BOOKS_COLLECTION)
            .document(bookId)
            .update(
                mapOf(
                    FIRESTORE_BOOK_IS_LENT to false,
                    FIRESTORE_BOOK_LENT_TO_USER_NAME to ""
                )
            )
        if (currentTab == 1) {
            getMyAvailableBooks()
        } else if (currentTab == 3) {
            getBooksThatIHaveLentFromSomeone()
        }
    }
}

fun DocumentSnapshot.toBook(): Book {
    return Book(
        documentId = this.id,
        label = this.data!![FIRESTORE_BOOK_LABEL].toString(),
        isLent = this.data!![FIRESTORE_BOOK_IS_LENT] as Boolean,
        ownerUserName = this.data!![FIRESTORE_BOOK_OWNER_USER_NAME].toString(),
        lentToUserName = this.data!![FIRESTORE_BOOK_LENT_TO_USER_NAME].toString()
    )
}
