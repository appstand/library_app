package com.appstand.library

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appstand.library.databinding.LayoutBookBinding
import com.appstand.library.model.Book


class BookAdapter(private val click: (documentId: String) -> Unit) :
    ListAdapter<Book, ViewHolder>(DiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutBookBinding = LayoutBookBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(itemBinding, click)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class ViewHolder(
    private val binding: LayoutBookBinding,
    private val click: (bookId: String) -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(book: Book) {
        binding.bookName.text = book.label
        binding.lentToName.text = book.lentToUserName
        binding.ownerName.text = book.ownerUserName
        binding.root.setOnClickListener { click.invoke(book.documentId) }
    }

}

private class DiffUtilCallback : DiffUtil.ItemCallback<Book>() {

    override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
        return oldItem == newItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Book, newItem: Book) = oldItem == newItem
}