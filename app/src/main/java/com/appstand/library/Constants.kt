package com.appstand.library

const val FIRESTORE_USERS_COLLECTION = "User"
const val FIRESTORE_USER_NAME = "userName"

const val FIRESTORE_BOOKS_COLLECTION = "Book"
const val FIRESTORE_BOOK_OWNER_USER_NAME = "ownerUserName"
const val FIRESTORE_BOOK_LENT_TO_USER_NAME = "lentToUserName"
const val FIRESTORE_BOOK_IS_LENT = "isLent"
const val FIRESTORE_BOOK_LABEL = "label"

const val FITESTORE_TRANSACTION_COLLECTION = "Transaction"
